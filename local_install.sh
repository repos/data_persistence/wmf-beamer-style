#!/bin/bash -e
#Install wmf-beamer.sty, logos to TEXMFHOME

if ! [ -x "$(which kpsewhich)" ]; then
    echo "Unable to find kpsewhich, giving up"
    exit 1
fi

kh=$(kpsewhich -var-value TEXMFHOME) || ( "Unable to find TEXMFHOME" ; exit 1 )

echo "Installing package and logos to $kh"
pp="${kh}/tex/latex/wmfbeamer"
mkdir -p "${pp}/logos"
cp wmf-beamer.sty "${pp}/"
cp logos/*.pdf "${pp}/logos/"
texhash "${kh}"

echo "Checking package now on search path"
cd ~
kpsewhich wmf-beamer.sty >/dev/null || ("Failed to find wmf-beamer.sty"; exit 1 )
echo "All done"
