* Wikimedia Foundation branding for Beamer / LaTeX

This is an implementation of the [[https://meta.wikimedia.org/wiki/Brand][Wikimedia Foundation branding]] for
[[https://en.wikipedia.org/wiki/Beamer_(LaTeX)][Beamer]], a popular LaTeX class for creating presentation slides.

For more information on using Beamer, including a tutorial,
please see [[http://mirrors.ctan.org/macros/latex/contrib/beamer/doc/beameruserguide.pdf][the Beamer user guide]].

For improvements to the style file or documentation (including the
example), please open issues or merge requests at
[[https://gitlab.wikimedia.org/repos/data_persistence/wmf-beamer-style][WMF gitlab]].

** Usage

Once installed, just add ~\usepackage{wmf-beamer}~ to your LaTeX
source file, and make slides as normal - don't specify any other
Beamer theme. The included [[./example.tex][example.tex]] file demonstrates the main
features of the package, including:
 - Title slide (title, author, date)
 - Macros for the WMF colours
 - Chapter title slides (using ~\part~)
 - Final "contact me" slide

You set your colour scheme up with
~\setbeamercolor{normal text}{fg=black,bg=white}~ (adjusted to taste),
and the correct logo colour is selected depending on whether you
specify black or white as the foreground colour.

This package only supports pdfLaTeX, due to a problem with the
sourceserifpro font package (see
[[https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1021166][Debian Bug 1021166]]
for details).

** Installation

Note that this project depends on the following packages: texlive-base, texlive-fonts-extra,
texlive-latex-base, texlive-latex-recommended, and texlive-pictures.

*** From repository

If you use Debian, the easiest approach is simply to install the
~wmf-beamer-style~ package from [[https://wikitech.wikimedia.org/wiki/APT_repository][The WMF Apt repository]].

*** From release zip file

Otherwise, if you've got this package as a released "zip" file, then
you need to arrange for ~wmf-beamer.sty~ and the logos to be somewhere
where pdfLaTeX can find them. The easiest way to achieve this is to run
the included [[./local_install.sh][local_install.sh]] which installs them into ~TEXMFHOME~
(i.e. the appropriate place in your home directory).

Release versions of the zip file can be found at 
[[https://gitlab.wikimedia.org/repos/data_persistence/wmf-beamer-style/-/releases][the releases tab]] 
of the gitlab page. Alternatively, you can download the in-development
version the CI built most recently here:
[[https://gitlab.wikimedia.org/repos/data_persistence/wmf-beamer-style/-/jobs/artifacts/main/raw/wmf-beamer-style.zip?job=build_zip]]

*** From source

To build from source, run ~make install~ (which builds PDF versions of
logos and then installs them and the style file into ~TEXMFHOME~) or
~make zip~ to build a release zipfile. Use ~dgit build~ or similar to
build a new Debian package.

Builds require ~inkscape~ (for the SVG to PDF conversion) and
~dpkg-dev~ (to parse the changelog file for the version to use in
constructing the zip filename).

** Licensing

Use of the Wikimedia Foundation logos (contained in the logos/
directory) is governed by [[https://foundation.wikimedia.org/wiki/Policy:Trademark_policy][The WMF Trademark policy]].  The rest of this
software is Copyright 2022 Wikimedia Foundation, and released under
the Apache License, Version 2.0. For more details, please see
[[./debian/copyright][debian/copyright]].
