#!/usr/bin/make -f

LOGOS=logos/Wikimedia_Foundation_logo_-_horizontal.pdf logos/Wikimedia_Foundation_logo_-_horizontal_white.pdf logos/Wikimedia-logo_black.pdf logos/Wikimedia-logo_white.pdf

%.pdf: %.svg
	inkscape $< -o $@

logos: $(LOGOS)

install: logos wmf-beamer.sty
	./local_install.sh

#run pdflatex twice to get cross-references &c right.
example.pdf: example.tex wmf-beamer.sty $(LOGOS)
	pdflatex example.tex
	pdflatex example.tex

zip: logos example.pdf
	./release_zip.sh

clean:
	rm -f logos/*.pdf example.{aux,log,nav,out,pdf,snm,toc,vrb}

.PHONY: clean install zip
