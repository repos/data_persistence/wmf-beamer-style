#!/bin/bash -e
#Make a zipfile suitable for standalone installation.

VERSION=$(dpkg-parsechangelog -S version)
cd ..
zip "wmf-beamer-style_${VERSION}.zip" wmf-beamer-style/example.{tex,pdf} wmf-beamer-style/wmf-beamer.sty wmf-beamer-style/local_install.sh wmf-beamer-style/README.org wmf-beamer-style/logos/*.pdf wmf-beamer-style/debian/copyright
